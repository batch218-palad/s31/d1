
const http = require("http");

// Creates a variable "port" to store port number
// 3000, 4000, 5000, 8000 - Usually used for web development
const port = 3000;

const server = http.createServer(function(request, response){
		// we will create two endpoint route for "/greeting/" and "/homepage" and will return a response upon accessing
		// The "url" property refers to the url or the link in the browser (endpoint)

		if(request.url == "/"){
			response.writeHead(200, {"Content-Type" : "text/plain"});
			response.end("Welcome to my page");
		}

		else if(request.url == "/greeting"){
			response.writeHead(200, {"Content-Type" : "text/plain"});
			response.end("Hello Again");
		}

// Mini Activity
	// Create another endpoint for the "/homepage" and send a response "You are now in my homepage".


		else if(request.url == "/homepage"){
			response.writeHead(200, {"Content-Type" : "text/plain"});
			response.end("You are now in my homepage");
		}

		else{ // block of code incase endpoint/url does not match
			response.writeHead(404, {"Content-Type" : "text/plain"});
			response.end("Page not available");
		}

});




server.listen(port);
console.log(`The server is now accessible at localhost: ${port}`);








